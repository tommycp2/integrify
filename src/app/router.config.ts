

import { Route } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { ProfileComponent } from "./profile/profile.component";
import { FaqComponent } from "./faq/faq.component";
import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";
import { TeamComponent } from './team/team.component';
import { InvestorsComponent } from './investors/investors.component';
import { ContactComponent } from './contact/contact.component';

import { PlaygroundComponent } from "./playground/playground.component";


export const routerConfig: Route[] = [
  {
    path: 'investors',
    component: InvestorsComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'playgound',
    component: PlaygroundComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'team',
    component: TeamComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: '**',
    redirectTo: 'home'
  },


]
