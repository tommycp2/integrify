import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Crowdfund1Component } from './crowdfund1.component';

describe('Crowdfund1Component', () => {
  let component: Crowdfund1Component;
  let fixture: ComponentFixture<Crowdfund1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Crowdfund1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Crowdfund1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
