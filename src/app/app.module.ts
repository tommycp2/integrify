import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { RouterModule } from "@angular/router";

import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';


import { routerConfig } from "./router.config";

import { environment } from '../environments/environment';

import 'hammerjs';

// components

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent, DialogResultExampleDialog } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { Crowdfund1Component } from './crowdfund1/crowdfund1.component';
import { PlaygroundComponent } from './playground/playground.component';
import { InvestorsComponent } from './investors/investors.component';

// services

import { ProfileService } from './services/profile.service';
import { TeamComponent } from './team/team.component';
import { ContactComponent } from './contact/contact.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    FaqComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent,
    Crowdfund1Component,
    DialogResultExampleDialog,
    PlaygroundComponent,
    InvestorsComponent,
    TeamComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    RouterModule.forRoot(routerConfig),
    AngularFireModule.initializeApp(environment.firebase, {
      provider: AuthProviders.Password,
      method: AuthMethods.Password,
    }),
  ],
  providers: [
    ProfileService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
