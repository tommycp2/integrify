/**
 * A service which holds user profile data from various sources
 */

import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';

export type UserData = {
  loginID: string,
  email: string,
  postcode: number,
} | null;

/**
 * the | null is just a typescrit formality, so that when we are checking for if there is an object present at UserData and if there is nothing there
 * the type is null instead of Userdata.
 */

type AuthData = {
  uid: string,
  auth: {
    email: string,
  },
};

@Injectable()
export class ProfileService {
  private userSubscription = null;
  private userData = null;

  constructor(private af: AngularFire) {
    af.auth.subscribe((auth: AuthData) => {
      if (auth) {
        this.watchUserDetails(auth);
      } else {
        this.userData = null;
        if (this.userSubscription) {
          this.userSubscription.unsubscribe();
          this.userSubscription = null;
        }
      }
    })
  }

  public isLoggedIn() {
    return this.userData !== null;
  }

  public getUserData(): UserData {
    return this.userData;
  }

  public login(email: string, password: string) {
    return this.af.auth.login({ email: email, password: password });
  }

  public register(email: string, password: string, postcode: string) {
    return this.af.auth.createUser({ email: email, password: password })
      .then((user: AuthData) => {
        const key = this.af.database.object(`users/${user.uid}`);
        key.set({
          postcode, email
        })
        return this.watchUserDetails(user);
      });
  }

  public logout() {
    this.af.auth.logout();
  }

  private watchUserDetails(authData: AuthData) {
    return new Promise((resolve, reject) => {
      this.userSubscription = this.af.database.object(`users/${authData.uid}`).subscribe(user => {
        if (!user) {
          return reject();
        }
        this.userData = {
          ...user,
          loginID: authData.uid,
          email: authData.auth.email,
        };
        resolve(this.userData);
      });
    });
  }
}
