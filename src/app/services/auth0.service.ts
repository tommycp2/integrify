// app/auth.service.ts

import { environment } from '../../environments/environment';

import { Injectable }      from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';

// Avoid name not found warnings, loaded from CDN
declare var Auth0Lock: any;

const { auth0 } = environment;

@Injectable()
export class AuthService {
  private dialogOptions = {
    auth: {
      redirectUrl: `${environment.baseURL}/profile`,
      responseType: 'token',
    },
  };

  // Configure Auth0
  private lock = new Auth0Lock(auth0.client, auth0.domain, {
    allowedConnections: ['Username-Password-Authentication'],
    autoclose: true,
    autofocus: true,
  });

  constructor() {
    // Add callback for lock `authenticated` event
    this.lock.on("authenticated", (authResult) => {
      localStorage.setItem('id_token', authResult.idToken);
    });
  }

  public login() {
    // Call the show method to display the widget.
    this.lock.show({
      ...this.dialogOptions,
      allowSignUp: false,
    });
  }

  public signup() {
    // Call the show method to display the widget.
    this.lock.show({
      ...this.dialogOptions,
      allowLogin: false,
    });
  }

  public authenticated() {
    // Check if there's an unexpired JWT
    // This searches for an item in localStorage with key == 'id_token'
    return tokenNotExpired();
  }

  public logout() {
    // Remove token from localStorage
    localStorage.removeItem('id_token');
  }
}
