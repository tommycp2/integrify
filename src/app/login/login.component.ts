import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // form input controllers
  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(public profile: ProfileService, public router: Router) {
  }

  ngOnInit() {
    if (this.profile.isLoggedIn()) {
      this.gotoProfilePage();
    }
  }

  public login() {
    this.profile.login(this.loginForm.value.email, this.loginForm.value.password)
      .then(() => this.gotoProfilePage());
  }

  private gotoProfilePage() {
    this.router.navigateByUrl('/profile');
  }
}




/*
if !user.postcode then take them to profile page so they can put in their post code,

*/
