import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { UserData } from '../services/profile.service';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  // form input controllers
  registerForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    postcode: new FormControl('', [Validators.required, Validators.pattern(/\d{4}/), Validators.maxLength(4)]),
  });

  constructor(public profile: ProfileService, public router: Router) {
  }

  ngOnInit() {
    if (this.profile.isLoggedIn()) {
      this.gotoPledgePage(this.profile.getUserData().postcode);
    }
  }

  public register() {
    const formValues = this.registerForm.value;
    this.profile.register(formValues.email, formValues.password, formValues.postcode)
      .then((user: UserData) => this.gotoPledgePage(user.postcode));
  }

  private gotoPledgePage(postcode) {
    this.router.navigateByUrl(`/status/${postcode}`);
  }
}

/* In here will will use the firebase authentication to create

using firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // ...

  Data join, uid and email address.
});

from https://firebase.google.com/docs/auth/web/password-auth

And we will also have a method that copies these the email address and uid and created a data join between them.
Watch Vasco's course on how to do Data Joins to work this out.

 */
